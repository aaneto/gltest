from app import App

from shapes import Rect

class MyApp(App):
    def setup(self):
        self.rect = Rect(self.context, self.program, (0.8, 0.8))
    
    def render(self):
        self.rect.render()

app = MyApp((800, 600), "Hello Window")

app.run()
