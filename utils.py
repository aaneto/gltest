import glfw

def read_file(filename):
    with open(filename, "r") as filep:
        content = filep.read()

    return content

def create_window(size, title, resize_callback):
    glfw.init()
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
    
    window = glfw.create_window(*size, title, None, None)
    glfw.make_context_current(window)
    glfw.set_framebuffer_size_callback(window, resize_callback)

    return window