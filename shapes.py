from abc import ABC
from abc import abstractmethod

import numpy
import moderngl

class Drawable(ABC):
    def __init__(self, context, program, vertex_data):
        self.vertex_data = vertex_data
        self.context = context
        self.program = program

        self.vertices = numpy.dstack(self.vertex_data)
        self.vbo = self.context.buffer(self.vertices.astype("f4").tobytes())
        self.vao = self.context.simple_vertex_array(self.program, self.vbo, "in_vert", "in_color")
    
    @abstractmethod
    def render(self):
        pass

class Rect(Drawable):
    def __init__(self, context, program, size):
        width, height = size
        
        half_width = width / 2
        half_height = height / 2

        xs = numpy.array([-half_width, half_width, half_width, -half_width])
        ys = numpy.array([half_height, half_height, -half_height, -half_height])
        r = numpy.array([0.0, 0.0, 1.0, 0.5])
        g = numpy.array([0.0, 1.0, 0.0, 0.3])
        b = numpy.array([1.0, 0.0, 0.0, 0.0])
        
        super().__init__(context, program, [xs, ys, r, g, b])
    
    def render(self):
        self.vao.render(moderngl.TRIANGLE_FAN)