import weakref
from abc import ABC
from abc import abstractmethod

import glfw
import moderngl

import utils

class App(ABC):
    def __init__(self, size, title):
        # Create context before configuring OpenGL
        self.window = utils.create_window(size, title, self.resize_window)
        self.configure_opengl()
        self.shapes = []
        self.setup()
    
    @abstractmethod
    def setup(self):
        pass
    
    @abstractmethod
    def render(self):
        pass
    
    def resize_window(self, window, width, height):
        self.context.viewport = (0, 0, width, height)

    def configure_opengl(self):
        """
        Configure OpenGL for drawing.

        This function must be called with a existent
        GL context for the process.
        """
        self.context = moderngl.create_context()
 
        self.program = self.context.program(
            vertex_shader=utils.read_file("shaders/vertex.shader"),
            fragment_shader=utils.read_file("shaders/fragment.shader"),
        )

        self.fbo = self.context.detect_framebuffer()
        self.fbo.use()
        
        # Cleanup glfw after self is collected
        weakref.finalize(self, glfw.terminate)
    
    def run(self):
        while not glfw.window_should_close(self.window):
            self.fbo.clear(0.0, 0.0, 0.0, 1.0)

            self.render()

            glfw.swap_buffers(self.window)
            glfw.poll_events()